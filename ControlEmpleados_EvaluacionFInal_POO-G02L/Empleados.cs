﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ControlEmpleados_EvaluacionFInal_POO_G02L
{
    public class Empleados
    {
        private List<Dictionary<string, object>> bdd;

        public Empleados()
        {
            this.bdd = new List<Dictionary<string, object>>() { };
        }

        public bool setEmpleado(
            string nombres, string apellidos, DateTime fecha_nacimiento, string telefono, string email,
            string dui, string nit, double sueldo, DateTime fecha_contrato, double renta, double isss
        )
        {
            if (this.bdd.Count == 10) return false;
            this.bdd.Add(new Dictionary<string, object>() {
                { "nombres", nombres }, { "apellidos", apellidos }, { "fecha_nacimiento", fecha_nacimiento }, { "telefono", telefono }, { "email", email },
                { "dui", dui }, { "nit", nit }, { "sueldo", sueldo }, { "fecha_contrato", fecha_contrato }, { "renta", renta }, { "isss", isss }
            });
            return true;
        }

        public List<Dictionary<string, object>> getEmpleados()
        {
            return this.bdd;
        }

        public int getTotalEmpleados()
        {
            return this.bdd.Count;
        }
    }
}
