﻿
namespace ControlEmpleados_EvaluacionFInal_POO_G02L
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ctrlEF_email = new System.Windows.Forms.TextBox();
            this.ctrlEF_telefono = new System.Windows.Forms.TextBox();
            this.ctrlEF_nacimiento = new System.Windows.Forms.DateTimePicker();
            this.ctrlEF_apellidos = new System.Windows.Forms.TextBox();
            this.ctrlEF_nombres = new System.Windows.Forms.TextBox();
            this.lblEF_email = new System.Windows.Forms.Label();
            this.lblEF_telefono = new System.Windows.Forms.Label();
            this.lblEF_nacimiento = new System.Windows.Forms.Label();
            this.lblEF_apellidos = new System.Windows.Forms.Label();
            this.lblEF_nombres = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ctrlEF_isss = new System.Windows.Forms.NumericUpDown();
            this.ctrlEF_renta = new System.Windows.Forms.NumericUpDown();
            this.ctrlEF_sueldo = new System.Windows.Forms.TextBox();
            this.ctrlEF_contrato = new System.Windows.Forms.DateTimePicker();
            this.ctrlEF_nit = new System.Windows.Forms.TextBox();
            this.ctrlEF_dui = new System.Windows.Forms.TextBox();
            this.lblEF_isss = new System.Windows.Forms.Label();
            this.lblEF_renta = new System.Windows.Forms.Label();
            this.lblEF_sueldo = new System.Windows.Forms.Label();
            this.lblEF_contrato = new System.Windows.Forms.Label();
            this.lblEF_nit = new System.Windows.Forms.Label();
            this.lblEF_dui = new System.Windows.Forms.Label();
            this.btnEF_nuevoE = new System.Windows.Forms.Button();
            this.btnEF_guardarE = new System.Windows.Forms.Button();
            this.btnEF_abrirP = new System.Windows.Forms.Button();
            this.btnEF_generarP = new System.Windows.Forms.Button();
            this.dgvEF_listaempleados = new System.Windows.Forms.DataGridView();
            this.colum1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlEF_isss)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlEF_renta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEF_listaempleados)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ctrlEF_email);
            this.groupBox1.Controls.Add(this.ctrlEF_telefono);
            this.groupBox1.Controls.Add(this.ctrlEF_nacimiento);
            this.groupBox1.Controls.Add(this.ctrlEF_apellidos);
            this.groupBox1.Controls.Add(this.ctrlEF_nombres);
            this.groupBox1.Controls.Add(this.lblEF_email);
            this.groupBox1.Controls.Add(this.lblEF_telefono);
            this.groupBox1.Controls.Add(this.lblEF_nacimiento);
            this.groupBox1.Controls.Add(this.lblEF_apellidos);
            this.groupBox1.Controls.Add(this.lblEF_nombres);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(500, 273);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Personales";
            // 
            // ctrlEF_email
            // 
            this.ctrlEF_email.Enabled = false;
            this.ctrlEF_email.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ctrlEF_email.Location = new System.Drawing.Point(16, 220);
            this.ctrlEF_email.Name = "ctrlEF_email";
            this.ctrlEF_email.Size = new System.Drawing.Size(469, 29);
            this.ctrlEF_email.TabIndex = 4;
            // 
            // ctrlEF_telefono
            // 
            this.ctrlEF_telefono.Enabled = false;
            this.ctrlEF_telefono.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ctrlEF_telefono.Location = new System.Drawing.Point(265, 139);
            this.ctrlEF_telefono.Name = "ctrlEF_telefono";
            this.ctrlEF_telefono.Size = new System.Drawing.Size(220, 29);
            this.ctrlEF_telefono.TabIndex = 3;
            this.ctrlEF_telefono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ctrlEF_telefono_KeyPress);
            // 
            // ctrlEF_nacimiento
            // 
            this.ctrlEF_nacimiento.CustomFormat = "dd-MM-yyyy";
            this.ctrlEF_nacimiento.Enabled = false;
            this.ctrlEF_nacimiento.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ctrlEF_nacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.ctrlEF_nacimiento.Location = new System.Drawing.Point(16, 137);
            this.ctrlEF_nacimiento.Name = "ctrlEF_nacimiento";
            this.ctrlEF_nacimiento.Size = new System.Drawing.Size(220, 29);
            this.ctrlEF_nacimiento.TabIndex = 2;
            this.ctrlEF_nacimiento.Value = new System.DateTime(2021, 11, 10, 15, 36, 59, 0);
            // 
            // ctrlEF_apellidos
            // 
            this.ctrlEF_apellidos.Enabled = false;
            this.ctrlEF_apellidos.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ctrlEF_apellidos.Location = new System.Drawing.Point(265, 63);
            this.ctrlEF_apellidos.Name = "ctrlEF_apellidos";
            this.ctrlEF_apellidos.Size = new System.Drawing.Size(220, 29);
            this.ctrlEF_apellidos.TabIndex = 1;
            this.ctrlEF_apellidos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ctrlEF_apellidos_KeyPress);
            // 
            // ctrlEF_nombres
            // 
            this.ctrlEF_nombres.Enabled = false;
            this.ctrlEF_nombres.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ctrlEF_nombres.Location = new System.Drawing.Point(16, 63);
            this.ctrlEF_nombres.Name = "ctrlEF_nombres";
            this.ctrlEF_nombres.Size = new System.Drawing.Size(220, 29);
            this.ctrlEF_nombres.TabIndex = 0;
            this.ctrlEF_nombres.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ctrlEF_nombres_KeyPress);
            // 
            // lblEF_email
            // 
            this.lblEF_email.AutoSize = true;
            this.lblEF_email.Location = new System.Drawing.Point(16, 194);
            this.lblEF_email.Name = "lblEF_email";
            this.lblEF_email.Size = new System.Drawing.Size(139, 18);
            this.lblEF_email.TabIndex = 4;
            this.lblEF_email.Text = "Correo Electrónico";
            // 
            // lblEF_telefono
            // 
            this.lblEF_telefono.AutoSize = true;
            this.lblEF_telefono.Location = new System.Drawing.Point(265, 108);
            this.lblEF_telefono.Name = "lblEF_telefono";
            this.lblEF_telefono.Size = new System.Drawing.Size(66, 18);
            this.lblEF_telefono.TabIndex = 3;
            this.lblEF_telefono.Text = "Teléfono";
            // 
            // lblEF_nacimiento
            // 
            this.lblEF_nacimiento.AutoSize = true;
            this.lblEF_nacimiento.Location = new System.Drawing.Point(16, 108);
            this.lblEF_nacimiento.Name = "lblEF_nacimiento";
            this.lblEF_nacimiento.Size = new System.Drawing.Size(135, 18);
            this.lblEF_nacimiento.TabIndex = 2;
            this.lblEF_nacimiento.Text = "Fecha Nacimiento";
            // 
            // lblEF_apellidos
            // 
            this.lblEF_apellidos.AutoSize = true;
            this.lblEF_apellidos.Location = new System.Drawing.Point(265, 38);
            this.lblEF_apellidos.Name = "lblEF_apellidos";
            this.lblEF_apellidos.Size = new System.Drawing.Size(73, 18);
            this.lblEF_apellidos.TabIndex = 1;
            this.lblEF_apellidos.Text = "Apellidos";
            // 
            // lblEF_nombres
            // 
            this.lblEF_nombres.AutoSize = true;
            this.lblEF_nombres.Location = new System.Drawing.Point(16, 38);
            this.lblEF_nombres.Name = "lblEF_nombres";
            this.lblEF_nombres.Size = new System.Drawing.Size(72, 18);
            this.lblEF_nombres.TabIndex = 0;
            this.lblEF_nombres.Text = "Nombres";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ctrlEF_isss);
            this.groupBox2.Controls.Add(this.ctrlEF_renta);
            this.groupBox2.Controls.Add(this.ctrlEF_sueldo);
            this.groupBox2.Controls.Add(this.ctrlEF_contrato);
            this.groupBox2.Controls.Add(this.ctrlEF_nit);
            this.groupBox2.Controls.Add(this.ctrlEF_dui);
            this.groupBox2.Controls.Add(this.lblEF_isss);
            this.groupBox2.Controls.Add(this.lblEF_renta);
            this.groupBox2.Controls.Add(this.lblEF_sueldo);
            this.groupBox2.Controls.Add(this.lblEF_contrato);
            this.groupBox2.Controls.Add(this.lblEF_nit);
            this.groupBox2.Controls.Add(this.lblEF_dui);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.groupBox2.Location = new System.Drawing.Point(539, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(500, 273);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos Laborales";
            // 
            // ctrlEF_isss
            // 
            this.ctrlEF_isss.DecimalPlaces = 1;
            this.ctrlEF_isss.Enabled = false;
            this.ctrlEF_isss.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ctrlEF_isss.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.ctrlEF_isss.Location = new System.Drawing.Point(266, 221);
            this.ctrlEF_isss.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.ctrlEF_isss.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ctrlEF_isss.Name = "ctrlEF_isss";
            this.ctrlEF_isss.Size = new System.Drawing.Size(220, 29);
            this.ctrlEF_isss.TabIndex = 10;
            this.ctrlEF_isss.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ctrlEF_renta
            // 
            this.ctrlEF_renta.DecimalPlaces = 1;
            this.ctrlEF_renta.Enabled = false;
            this.ctrlEF_renta.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ctrlEF_renta.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.ctrlEF_renta.Location = new System.Drawing.Point(19, 221);
            this.ctrlEF_renta.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.ctrlEF_renta.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ctrlEF_renta.Name = "ctrlEF_renta";
            this.ctrlEF_renta.Size = new System.Drawing.Size(220, 29);
            this.ctrlEF_renta.TabIndex = 9;
            this.ctrlEF_renta.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ctrlEF_sueldo
            // 
            this.ctrlEF_sueldo.Enabled = false;
            this.ctrlEF_sueldo.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ctrlEF_sueldo.Location = new System.Drawing.Point(266, 139);
            this.ctrlEF_sueldo.Name = "ctrlEF_sueldo";
            this.ctrlEF_sueldo.Size = new System.Drawing.Size(220, 29);
            this.ctrlEF_sueldo.TabIndex = 8;
            this.ctrlEF_sueldo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ctrlEF_sueldo_KeyPress);
            // 
            // ctrlEF_contrato
            // 
            this.ctrlEF_contrato.CustomFormat = "dd-MM-yyyy";
            this.ctrlEF_contrato.Enabled = false;
            this.ctrlEF_contrato.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ctrlEF_contrato.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.ctrlEF_contrato.Location = new System.Drawing.Point(19, 139);
            this.ctrlEF_contrato.Name = "ctrlEF_contrato";
            this.ctrlEF_contrato.Size = new System.Drawing.Size(220, 29);
            this.ctrlEF_contrato.TabIndex = 7;
            this.ctrlEF_contrato.Value = new System.DateTime(2021, 11, 10, 15, 36, 59, 0);
            // 
            // ctrlEF_nit
            // 
            this.ctrlEF_nit.Enabled = false;
            this.ctrlEF_nit.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ctrlEF_nit.Location = new System.Drawing.Point(266, 63);
            this.ctrlEF_nit.Name = "ctrlEF_nit";
            this.ctrlEF_nit.Size = new System.Drawing.Size(220, 29);
            this.ctrlEF_nit.TabIndex = 6;
            this.ctrlEF_nit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ctrlEF_nit_KeyPress);
            // 
            // ctrlEF_dui
            // 
            this.ctrlEF_dui.Enabled = false;
            this.ctrlEF_dui.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ctrlEF_dui.Location = new System.Drawing.Point(19, 63);
            this.ctrlEF_dui.Name = "ctrlEF_dui";
            this.ctrlEF_dui.Size = new System.Drawing.Size(220, 29);
            this.ctrlEF_dui.TabIndex = 5;
            this.ctrlEF_dui.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ctrlEF_dui_KeyPress);
            // 
            // lblEF_isss
            // 
            this.lblEF_isss.AutoSize = true;
            this.lblEF_isss.Location = new System.Drawing.Point(266, 194);
            this.lblEF_isss.Name = "lblEF_isss";
            this.lblEF_isss.Size = new System.Drawing.Size(123, 18);
            this.lblEF_isss.TabIndex = 10;
            this.lblEF_isss.Text = "Descuento ISSS";
            // 
            // lblEF_renta
            // 
            this.lblEF_renta.AutoSize = true;
            this.lblEF_renta.Location = new System.Drawing.Point(19, 194);
            this.lblEF_renta.Name = "lblEF_renta";
            this.lblEF_renta.Size = new System.Drawing.Size(128, 18);
            this.lblEF_renta.TabIndex = 9;
            this.lblEF_renta.Text = "Descuento Renta";
            // 
            // lblEF_sueldo
            // 
            this.lblEF_sueldo.AutoSize = true;
            this.lblEF_sueldo.Location = new System.Drawing.Point(266, 108);
            this.lblEF_sueldo.Name = "lblEF_sueldo";
            this.lblEF_sueldo.Size = new System.Drawing.Size(57, 18);
            this.lblEF_sueldo.TabIndex = 8;
            this.lblEF_sueldo.Text = "Sueldo";
            // 
            // lblEF_contrato
            // 
            this.lblEF_contrato.AutoSize = true;
            this.lblEF_contrato.Location = new System.Drawing.Point(19, 108);
            this.lblEF_contrato.Name = "lblEF_contrato";
            this.lblEF_contrato.Size = new System.Drawing.Size(116, 18);
            this.lblEF_contrato.TabIndex = 7;
            this.lblEF_contrato.Text = "Fecha Contrato";
            // 
            // lblEF_nit
            // 
            this.lblEF_nit.AutoSize = true;
            this.lblEF_nit.Location = new System.Drawing.Point(266, 38);
            this.lblEF_nit.Name = "lblEF_nit";
            this.lblEF_nit.Size = new System.Drawing.Size(31, 18);
            this.lblEF_nit.TabIndex = 6;
            this.lblEF_nit.Text = "NIT";
            // 
            // lblEF_dui
            // 
            this.lblEF_dui.AutoSize = true;
            this.lblEF_dui.Location = new System.Drawing.Point(19, 38);
            this.lblEF_dui.Name = "lblEF_dui";
            this.lblEF_dui.Size = new System.Drawing.Size(34, 18);
            this.lblEF_dui.TabIndex = 5;
            this.lblEF_dui.Text = "DUI";
            // 
            // btnEF_nuevoE
            // 
            this.btnEF_nuevoE.BackColor = System.Drawing.Color.DarkRed;
            this.btnEF_nuevoE.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEF_nuevoE.FlatAppearance.BorderColor = System.Drawing.Color.DarkRed;
            this.btnEF_nuevoE.FlatAppearance.BorderSize = 2;
            this.btnEF_nuevoE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEF_nuevoE.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnEF_nuevoE.ForeColor = System.Drawing.Color.White;
            this.btnEF_nuevoE.Location = new System.Drawing.Point(28, 304);
            this.btnEF_nuevoE.Name = "btnEF_nuevoE";
            this.btnEF_nuevoE.Size = new System.Drawing.Size(220, 49);
            this.btnEF_nuevoE.TabIndex = 2;
            this.btnEF_nuevoE.Text = "Nuevo Empleado";
            this.btnEF_nuevoE.UseVisualStyleBackColor = false;
            this.btnEF_nuevoE.Click += new System.EventHandler(this.btnEF_nuevoE_Click);
            // 
            // btnEF_guardarE
            // 
            this.btnEF_guardarE.BackColor = System.Drawing.Color.DarkRed;
            this.btnEF_guardarE.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEF_guardarE.Enabled = false;
            this.btnEF_guardarE.FlatAppearance.BorderColor = System.Drawing.Color.DarkRed;
            this.btnEF_guardarE.FlatAppearance.BorderSize = 2;
            this.btnEF_guardarE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEF_guardarE.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnEF_guardarE.ForeColor = System.Drawing.Color.White;
            this.btnEF_guardarE.Location = new System.Drawing.Point(277, 304);
            this.btnEF_guardarE.Name = "btnEF_guardarE";
            this.btnEF_guardarE.Size = new System.Drawing.Size(220, 49);
            this.btnEF_guardarE.TabIndex = 3;
            this.btnEF_guardarE.Text = "Guardar Empleado";
            this.btnEF_guardarE.UseVisualStyleBackColor = false;
            this.btnEF_guardarE.Click += new System.EventHandler(this.btnEF_guardarE_Click);
            // 
            // btnEF_abrirP
            // 
            this.btnEF_abrirP.BackColor = System.Drawing.Color.DarkRed;
            this.btnEF_abrirP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEF_abrirP.Enabled = false;
            this.btnEF_abrirP.FlatAppearance.BorderColor = System.Drawing.Color.DarkRed;
            this.btnEF_abrirP.FlatAppearance.BorderSize = 2;
            this.btnEF_abrirP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEF_abrirP.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnEF_abrirP.ForeColor = System.Drawing.Color.White;
            this.btnEF_abrirP.Location = new System.Drawing.Point(558, 304);
            this.btnEF_abrirP.Name = "btnEF_abrirP";
            this.btnEF_abrirP.Size = new System.Drawing.Size(220, 49);
            this.btnEF_abrirP.TabIndex = 4;
            this.btnEF_abrirP.Text = "Abrir Planilla";
            this.btnEF_abrirP.UseVisualStyleBackColor = false;
            this.btnEF_abrirP.Click += new System.EventHandler(this.btnEF_abrirP_Click);
            // 
            // btnEF_generarP
            // 
            this.btnEF_generarP.BackColor = System.Drawing.Color.DarkRed;
            this.btnEF_generarP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEF_generarP.Enabled = false;
            this.btnEF_generarP.FlatAppearance.BorderColor = System.Drawing.Color.DarkRed;
            this.btnEF_generarP.FlatAppearance.BorderSize = 2;
            this.btnEF_generarP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEF_generarP.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnEF_generarP.ForeColor = System.Drawing.Color.White;
            this.btnEF_generarP.Location = new System.Drawing.Point(805, 304);
            this.btnEF_generarP.Name = "btnEF_generarP";
            this.btnEF_generarP.Size = new System.Drawing.Size(220, 49);
            this.btnEF_generarP.TabIndex = 5;
            this.btnEF_generarP.Text = "Generar Planilla";
            this.btnEF_generarP.UseVisualStyleBackColor = false;
            this.btnEF_generarP.Click += new System.EventHandler(this.btnEF_generarP_Click);
            // 
            // dgvEF_listaempleados
            // 
            this.dgvEF_listaempleados.AllowUserToAddRows = false;
            this.dgvEF_listaempleados.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.InfoText;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(2);
            this.dgvEF_listaempleados.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvEF_listaempleados.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEF_listaempleados.BackgroundColor = System.Drawing.Color.White;
            this.dgvEF_listaempleados.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DarkRed;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(1);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            this.dgvEF_listaempleados.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvEF_listaempleados.ColumnHeadersHeight = 45;
            this.dgvEF_listaempleados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvEF_listaempleados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colum1,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.dgvEF_listaempleados.Enabled = false;
            this.dgvEF_listaempleados.EnableHeadersVisualStyles = false;
            this.dgvEF_listaempleados.GridColor = System.Drawing.Color.White;
            this.dgvEF_listaempleados.Location = new System.Drawing.Point(13, 380);
            this.dgvEF_listaempleados.Name = "dgvEF_listaempleados";
            this.dgvEF_listaempleados.ReadOnly = true;
            this.dgvEF_listaempleados.RowHeadersVisible = false;
            this.dgvEF_listaempleados.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvEF_listaempleados.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvEF_listaempleados.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.dgvEF_listaempleados.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgvEF_listaempleados.RowTemplate.DefaultCellStyle.Padding = new System.Windows.Forms.Padding(1);
            this.dgvEF_listaempleados.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.DimGray;
            this.dgvEF_listaempleados.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEF_listaempleados.RowTemplate.Height = 25;
            this.dgvEF_listaempleados.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvEF_listaempleados.Size = new System.Drawing.Size(1026, 314);
            this.dgvEF_listaempleados.TabIndex = 6;
            // 
            // colum1
            // 
            this.colum1.HeaderText = "Nombre Empleado";
            this.colum1.Name = "colum1";
            this.colum1.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Monto Renta";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Monto ISSS";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Total Descuentos";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Sueldo Base";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Sueldo Neto";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1050, 706);
            this.Controls.Add(this.dgvEF_listaempleados);
            this.Controls.Add(this.btnEF_generarP);
            this.Controls.Add(this.btnEF_abrirP);
            this.Controls.Add(this.btnEF_guardarE);
            this.Controls.Add(this.btnEF_nuevoE);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Registro Inicial de Empleados";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlEF_isss)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlEF_renta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEF_listaempleados)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblEF_email;
        private System.Windows.Forms.Label lblEF_telefono;
        private System.Windows.Forms.Label lblEF_nacimiento;
        private System.Windows.Forms.Label lblEF_apellidos;
        private System.Windows.Forms.Label lblEF_nombres;
        private System.Windows.Forms.Label lblEF_isss;
        private System.Windows.Forms.Label lblEF_renta;
        private System.Windows.Forms.Label lblEF_sueldo;
        private System.Windows.Forms.Label lblEF_contrato;
        private System.Windows.Forms.Label lblEF_nit;
        private System.Windows.Forms.Label lblEF_dui;
        private System.Windows.Forms.TextBox ctrlEF_apellidos;
        private System.Windows.Forms.TextBox ctrlEF_nombres;
        private System.Windows.Forms.TextBox ctrlEF_telefono;
        private System.Windows.Forms.DateTimePicker ctrlEF_nacimiento;
        private System.Windows.Forms.TextBox ctrlEF_email;
        private System.Windows.Forms.TextBox ctrlEF_nit;
        private System.Windows.Forms.TextBox ctrlEF_dui;
        private System.Windows.Forms.DateTimePicker ctrlEF_contrato;
        private System.Windows.Forms.NumericUpDown ctrlEF_renta;
        private System.Windows.Forms.TextBox ctrlEF_sueldo;
        private System.Windows.Forms.NumericUpDown ctrlEF_isss;
        private System.Windows.Forms.Button btnEF_nuevoE;
        private System.Windows.Forms.Button btnEF_guardarE;
        private System.Windows.Forms.Button btnEF_abrirP;
        private System.Windows.Forms.Button btnEF_generarP;
        private System.Windows.Forms.DataGridView dgvEF_listaempleados;
        private System.Windows.Forms.DataGridViewTextBoxColumn colum1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
    }
}

