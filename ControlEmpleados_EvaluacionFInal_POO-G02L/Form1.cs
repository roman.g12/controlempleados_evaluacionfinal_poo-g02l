﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlEmpleados_EvaluacionFInal_POO_G02L
{
    public partial class Form1 : Form
    {
        public Empleados emp;
        public Form1()
        {
            InitializeComponent();
            this.emp = new Empleados();
        }

        private void btnEF_nuevoE_Click(object sender, EventArgs e)
        {
            habilitarCamposNuevoEmpleado();
        }

        private void btnEF_abrirP_Click(object sender, EventArgs e)
        {
            btnEF_generarP.Enabled = true;
            btnEF_abrirP.Enabled = false;
        }

        private void btnEF_generarP_Click(object sender, EventArgs e)
        {
            fillEmpleado();
        }

        private void btnEF_guardarE_Click(object sender, EventArgs e)
        {
            //Validamos que no queden los campos vacíos
            if (
                ctrlEF_nombres.Text.Trim() != "" && ctrlEF_apellidos.Text.Trim() != "" &&
                ctrlEF_telefono.Text.Trim() != "" && ctrlEF_nacimiento.Text.Trim() != "" &&
                ctrlEF_email.Text.Trim() != "" && ctrlEF_dui.Text.Trim() != "" && ctrlEF_nit.Text.Trim() != "" &&
                ctrlEF_sueldo.Text.Trim() != "" && ctrlEF_isss.Text.Trim() != "" &&
                ctrlEF_contrato.Text.Trim() != "" && ctrlEF_renta.Text.Trim() != ""
            )
            {
                //Llamamos función que valide solo letras
                if (validarSoloLetras(ctrlEF_nombres.Text, ctrlEF_apellidos.Text) == 1) {
                    //Llamamos función que valide fecha de naicimiento
                    if (validarEdad() == 1) {
                        //Llamamos función que valide número de teléfono
                        if (validarNumeroTelefono(ctrlEF_telefono.Text) == 1) {
                            //Llamamos función que valide correo electrónico
                            if (validarEmail(ctrlEF_email.Text) == 1) {
                                //Llamamos función que valide DUI
                                if (validarDui(ctrlEF_dui.Text) == 1) {
                                    //Llamamos función que valide NIT
                                    if (validarNit(ctrlEF_nit.Text) == 1) {
                                        //Llamamos función que valide fecha de contrato
                                        if (validarFechaContrato() == 1) {
                                            //Llamamos función que valide sueldo
                                            if (validarSueldo(ctrlEF_sueldo.Text) == 1) {
                                                guardarEmpleado(
                                                    ctrlEF_nombres.Text.Trim(), ctrlEF_apellidos.Text.Trim(), ctrlEF_nacimiento.Value,
                                                    ctrlEF_telefono.Text.Trim(), ctrlEF_email.Text.Trim(),
                                                    ctrlEF_dui.Text.Trim(), ctrlEF_nit.Text.Trim(),
                                                    ctrlEF_contrato.Value, double.Parse(ctrlEF_sueldo.Text.Trim(), System.Globalization.CultureInfo.InvariantCulture), //Convertimos al sitema de decimales
                                                    (Decimal.ToDouble(ctrlEF_isss.Value) / 100), (Decimal.ToDouble(ctrlEF_renta.Value) / 100)
                                                );
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else {
                MessageBox.Show("No puede dejar campos vacíos",
                    "Formulario Empleados", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //FUNCIONES PARA GUARDAR Y OBTENER DATOS -----------------------------------------------------------------------------------------------

        private void guardarEmpleado(
            string nombres, string apellidos, DateTime fecha_nacimiento,
            string telefono, string email,
            string dui, string nit,
            DateTime fecha_contrato, double sueldo,
            double isss, double renta
        )
        {
            if (this.emp.setEmpleado(
                nombres, apellidos, fecha_nacimiento, telefono, email,
                dui, nit, sueldo, fecha_contrato, renta, isss
            ))
            {
                MessageBox.Show("Empleado agregado.",
                    "Formulario Empleados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpiarCampos();
                btnEF_nuevoE.Enabled = true;
                btnEF_guardarE.Enabled = false;
                if (this.emp.getTotalEmpleados() == 10)
                {
                    limpiarCampos();
                    deshabilitarCamposNuevoEmpleado();
                    btnEF_abrirP.Enabled = true;
                }
            }
            else
            {
                MessageBox.Show("Ya se han agregado los 10 empleados.",
                    "Formulario Empleados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpiarCampos();
                deshabilitarCamposNuevoEmpleado();
                btnEF_abrirP.Enabled = true;
            }
        }

        private void fillEmpleado()
        {
            List<Dictionary<string, object>> auxlist = this.emp.getEmpleados();
            foreach (Dictionary<string, object> item in auxlist)
            {
                dgvEF_listaempleados.Rows.Add(
                    (string)item["nombres"] + " " + (string)item["apellidos"],
                    (double)item["sueldo"] * (double)item["renta"],
                    (double)item["sueldo"] * (double)item["isss"],
                    ((double)item["sueldo"] * (double)item["renta"]) + ((double)item["sueldo"] * (double)item["isss"]),
                    (double)item["sueldo"],
                    (double)item["sueldo"] - (((double)item["sueldo"] * (double)item["renta"]) + ((double)item["sueldo"] * (double)item["isss"]))
                );
            }
        }

        //FUNCIONES PARA VALIDAR CAMPOS --------------------------------------------------------------------------------------------------------

        private int validarEdad() {
            int edad;
            int edadMayor = 18;
            int edadMenor = 50;

            //Obtenemos la fecha de nacimiento
            DateTime fechaNacimiento = Convert.ToDateTime(ctrlEF_nacimiento.Text);

            //Obtenemos la fecha actual
            DateTime fechaActual = DateTime.Today;

            //Calculamos la edad
            edad = fechaActual.Year - fechaNacimiento.Year;

            //Validamos que la edad sea mayor a 18 y menor a 50
            if (edad >= edadMayor && edad <= edadMenor)
            {
                return 1;
            }
            else {
                MessageBox.Show("No es posible registrar usuarios menores de 18 o mayores de 50",
                    "Formulario Empleados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
        }

        private int validarFechaContrato() {
            //la fecha de contrato debe ser menor o igual a la fecha actual
            DateTime fechaContrato = Convert.ToDateTime(ctrlEF_contrato.Text);
            DateTime fechaActual = DateTime.Today;

            if (fechaContrato <= fechaActual) {
                return 1;
            }
            else {
                MessageBox.Show("La fecha de contrato no puede ser mayor a la fecha actual",
                    "Formulario Empleados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
        }

        private int validarNumeroTelefono(string telefono) {
            //regex que contenga 4 digitos un guión y 4 digitos
            string regex = @"^\d{4}-\d{4}$";

            if (Regex.IsMatch(telefono, regex))
            {
                return 1;
            }
            else {
                MessageBox.Show("El número de teléfono no es válido -- Formato (####-####)",
                    "Formulario Empleados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
        }

        private int validarSoloLetras(string nombres, string apellidos)
        {
            //regex que contenga solo letras y letras con acentos
            string regex = @"^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$";

            if (Regex.IsMatch(nombres, regex) && Regex.IsMatch(apellidos, regex))
            {
                return 1;
            }
            else {
                MessageBox.Show("Los campos nombre y apellidos solo aceptan letras",
                    "Formulario Empleados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
        }

        private int validarEmail(string email) {
            //regex validar email
            string regex = @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";

            if (Regex.IsMatch(email, regex))
            {
                return 1;
            }
            else {
                MessageBox.Show("El correo electrónico no es válido",
                    "Formulario Empleados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
        }

        private int validarDui(string dui) {
            //regex validar dui
            string regex = @"^\d{8}-\d{1}$";

            if (Regex.IsMatch(dui, regex))
            {
                return 1;
            }
            else {
                MessageBox.Show("El DUI no es válido -- Formato (########-#)",
                    "Formulario Empleados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
        }

        private int validarNit(string nit) {
            //regex validar nit
            string regex = @"^\d{4}-\d{6}-\d{3}-\d{1}$";

            if (Regex.IsMatch(nit, regex))
            {
                return 1;
            }
            else {
                MessageBox.Show("El NIT no es válido -- Formato (####-######-###-#)",
                    "Formulario Empleados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
        }

        private int validarSueldo(string sueldo) {
            string regex = @"^\d{1,9}(\.\d{2})?$";

            if (Regex.IsMatch(sueldo, regex))
            {
                return 1;
            }
            else {
                MessageBox.Show("No agregue más de dos decimales en el sueldo",
                    "Formulario Empleados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
        }

        //FIN DE LA SECCIÓN DE FUNCIONES PARA VALIDAR CAMPOS -----------------------------------------------------------------------------------------

        private void habilitarCamposNuevoEmpleado()
        {
            ctrlEF_nombres.Enabled = true;
            ctrlEF_apellidos.Enabled = true;
            ctrlEF_telefono.Enabled = true;
            ctrlEF_nacimiento.Enabled = true;
            ctrlEF_email.Enabled = true;
            ctrlEF_dui.Enabled = true;
            ctrlEF_nit.Enabled = true;
            ctrlEF_sueldo.Enabled = true;
            ctrlEF_isss.Enabled = true;
            ctrlEF_contrato.Enabled = true;
            ctrlEF_renta.Enabled = true;
            btnEF_guardarE.Enabled = true;
            btnEF_nuevoE.Enabled = false;
        }
        private void deshabilitarCamposNuevoEmpleado()
        {
            ctrlEF_nombres.Enabled = false;
            ctrlEF_apellidos.Enabled = false;
            ctrlEF_telefono.Enabled = false;
            ctrlEF_nacimiento.Enabled = false;
            ctrlEF_email.Enabled = false;
            ctrlEF_dui.Enabled = false;
            ctrlEF_nit.Enabled = false;
            ctrlEF_sueldo.Enabled = false;
            ctrlEF_isss.Enabled = false;
            ctrlEF_contrato.Enabled = false;
            ctrlEF_renta.Enabled = false;
            btnEF_guardarE.Enabled = false;
            btnEF_nuevoE.Enabled = false;
        }

        private void limpiarCampos() 
        {
            ctrlEF_nombres.Text = "";
            ctrlEF_apellidos.Text = "";
            ctrlEF_telefono.Text = "";
            ctrlEF_nacimiento.Text = "";
            ctrlEF_email.Text = "";
            ctrlEF_dui.Text = "";
            ctrlEF_nit.Text = "";
            ctrlEF_sueldo.Text = "";
            ctrlEF_isss.Text = "";
            ctrlEF_contrato.Text = "";
            ctrlEF_renta.Text = "";
        }

        private void ctrlEF_sueldo_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Solo númros máximo un decimal
            if (e.KeyChar == '.' && ctrlEF_sueldo.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
            }
            else if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void ctrlEF_dui_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Solo números y guiones
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '-')
            {
                e.Handled = true;
            }
        }

        private void ctrlEF_nit_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Solo números y guiones
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '-')
            {
                e.Handled = true;
            }
        }

        private void ctrlEF_nombres_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Solo letras
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void ctrlEF_apellidos_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Solo letras
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void ctrlEF_telefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Solo números y guiones
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '-')
            {
                e.Handled = true;
            }
        }

    }
}
